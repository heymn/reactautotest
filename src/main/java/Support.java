import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by heymn on 10.12.2019.
 */
public class Support {

    //получение ключа для тестирования
    public String browser = System.getProperty("browser");
    public WebDriver driver;
    public Support() {
        getDriver(browser);
    }

    // выбор браузера для тестирования по ключу
    public void getDriver(String type){
        String driverPath = "C:\\Users\\heymn\\IdeaProjects\\reactAutotest\\src\\main\\resources\\drivers\\";
        switch (type){
            case "ch":
                System.setProperty("webdriver.chrome.driver", driverPath+"chromedriver.exe");
                driver = new ChromeDriver();
                break;
            case "ff":
                System.setProperty("webdriver.gecko.driver", driverPath+"geckodriver.exe");
                DesiredCapabilities dc = new DesiredCapabilities();
                dc.setCapability("marionatte", false);
                FirefoxOptions opt = new FirefoxOptions();
                opt.merge(dc);
                driver = new FirefoxDriver(opt);
                break;
            default:
                break;
        }
        driver.manage().window().maximize();
    }

    //запуск браузера
    public void go (String url){
        driver.get(url);
        //задержка, необходимая для отработки js(react)
        //это костыль, я знаю...дёшего и сердито
        sleep(4000);
    }

    //функция задержки
    public void sleep(int t){
        try {
            Thread.sleep(t);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    //получение данных с элемента формы
    public String getData(WebElement element){
        return element.getText().trim();
    }

    //передача данных в форму
    public void setData(WebElement element, String data){
        element.sendKeys(data);
    }

    //доступ к элементу формы по его селектору
    public WebElement getEl(String path){
        return driver.findElement(By.xpath(path));
    }

    //даблклик мыши на элемент формы
    public void doubleClick(WebElement element){
        Actions action = new Actions(driver);
        action.doubleClick(element).perform();
    }

    //клик мыши на элемент формы
    public void click (WebElement element){
        Actions action = new Actions(driver);
        action.click(element).perform();
    }

    //перемещение курсора на элемент формы
    public void moveCursor (WebElement element){
        Actions action = new Actions(driver);
        action.moveToElement(element).build().perform();
        sleep(3000);
    }

    //нажатие на клавишу Enter
    public void enterKey (WebElement element){
        try{
            element.sendKeys(Keys.ENTER);
        } catch (Exception ex){
        ex.printStackTrace();
        }
    }

    //остановка работы Selenium
    public void stopDriver(){
        driver.quit();
    }

    //перегрузка страницы
    public void refreshPage(){
        driver.navigate().refresh();
        //задержка, необходимая для отработки js(react)
        //это костыль, я знаю...дёшего и сердито
        sleep(3000);
    }



}
