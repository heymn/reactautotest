import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heymn on 10.12.2019.
 */
public class Main {

    //Список выполненных тестов и их результат
    private static List <String> testResult = new ArrayList<>();

    public static void main(String[] args) {

        //Массив проверочных значений для Теста 4
        String[] checkArrayTest3 = {"Test 1", "Test 2","Test 3"};

        //Запуск самих тестов
        test1AddNote("Test 1");
        test2RewritingNote("Test 2", " Complete");
        test3DeleteNote("Test 3");
        test4SaveNotes(checkArrayTest3);

        //Вывод результата в консоль
        testResult.forEach(System.out::println);
    }

    //Тест: ввод данных в списоке
    public static void test1AddNote (String testWord){

        Support support = new Support();

        try {
            //Инициализация браузера для проверки
            support.go("http://todomvc.com/examples/react/#/");

            //Ввод данных в список
            WebElement insertElement = support.getEl("//input[contains(@class,'new-todo')]");
            support.setData(insertElement, testWord);
            support.enterKey(insertElement);

            //Проверяем, что записалось в список
            String checkWord = support.getData(support.getEl("//ul[contains(@class,'todo-list')]/li[1]//label"));

            //Если результаты входных и полученных данных одинаковы, то тест пройден
            if (testWord.equals(checkWord)) {
                testResult.add("Test 1: Success");
            } else {
                testResult.add("Test 1: Error");
            }

            //Выключаем вебдрайвер
            support.stopDriver();
        } catch (Exception e) {
            testResult.add("Test 1: Exception");
            e.printStackTrace();
            support.stopDriver();
        }

    }

    //Тест: Редактирование существующей записи
    public static void test2RewritingNote(String testWord1, String testWord2){

        Support support = new Support();

        try {
            //Инициализация браузера для проверки
            support.go("http://todomvc.com/examples/react/#/");

            //Ввод данных в список
            WebElement insertElement = support.getEl("//input[contains(@class,'new-todo')]");
            support.setData(insertElement, testWord1);
            support.enterKey(insertElement);

            //Открытие компонента на редактирование
            support.doubleClick(support.getEl("//ul[contains(@class,'todo-list')]/li[1]"));

            //Ввод данных в список
            WebElement editElement = support.getEl("//ul[contains(@class,'todo-list')]/li[1]//input[@class='edit']");
            support.setData(editElement, testWord2);
            support.enterKey(editElement);

            //Проверяем, что записалось в список
            String checkWord = support.getData(support.getEl("//ul[contains(@class,'todo-list')]/li[1]//label"));

            //Если результаты входных данных после изменерия и полученных данных одинаковы, то тест прошёл
            if (checkWord.equals(testWord1 + testWord2)) {
                testResult.add("Test 2: Success");
            } else {
                testResult.add("Test 2: Error");
            }

            //Выключаем вебдрайвер
            support.stopDriver();
        } catch (Exception e) {
            testResult.add("Test 2: Exception");
            e.printStackTrace();
            support.stopDriver();
        }

    }

    public static void test3DeleteNote (String testWord){

        Support support = new Support();

        try {
            //Инициализация браузера для проверки
            support.go("http://todomvc.com/examples/react/#/");

            //Проверяем, сколько элементов сейчас в списке
            List<WebElement> listControl = support.driver.findElements(By.xpath("//ul[contains(@class,'todo-list')]/li"));

            //Ввод данных в список
            WebElement insertElement = support.getEl("//input[contains(@class,'new-todo')]");
            support.setData(insertElement, testWord);
            support.enterKey(insertElement);

            //Удаляем элемент из списка
            support.moveCursor(support.getEl("//ul[contains(@class,'todo-list')]/li[1]"));
            support.click(support.getEl("//ul[contains(@class,'todo-list')]/li[1]//button[@class='destroy']"));

            //Проверяем, сколько элементов сейчас в списке
            List<WebElement> listTest = support.driver.findElements(By.xpath("//ul[contains(@class,'todo-list')]/li"));

            //Если количество элементов после добавления + удаления в списке не изменилось - тест пройден
            if ((listTest.size() - listControl.size()) == 0) {
                testResult.add("Test 3: Success");
            } else {
                testResult.add("Test 3: Error");
            }

            //Выключаем вебдрайвер
            support.stopDriver();
        } catch (Exception e) {
            testResult.add("Test 3: Exception");
            e.printStackTrace();
            support.stopDriver();
        }


    }

    //Тест на сохранение вбитого списка после перезагрузки страницы
    public static void test4SaveNotes (String[] controlArray){

        Support support = new Support();

        try {
            //Инициализация браузера для проверки
            support.go("http://todomvc.com/examples/react/#/");

            //Ввод данных в список
            WebElement insertElement = support.getEl("//input[contains(@class,'new-todo')]");
            for (String aControlArray : controlArray) {
                support.setData(insertElement, aControlArray);
                support.enterKey(insertElement);
            }

            //Проверяем, сколько элементов сейчас в списке
            List<WebElement> listControl = support.driver.findElements(By.xpath("//ul[contains(@class,'todo-list')]/li"));
            //Перезагружаем в страницу
            support.refreshPage();
            //Проверяем, сколько элементов сейчас в списке
            List<WebElement> listTest = support.driver.findElements(By.xpath("//ul[contains(@class,'todo-list')]/li"));

            //Проверяем, что все элементы в списке остались на своих местах и не изменились
            boolean sameElement = false;
            for (int i = 1; i <= listControl.size(); i++) {
                String checkWord = support.getData(support.getEl("//ul[contains(@class,'todo-list')]/li[" + i + "]//label"));
                sameElement = checkWord.equals(controlArray[i - 1]);
                if (!sameElement) {
                    break;
                }
            }

            //Если количество элементов после перезагрузки = количеству до + это те же элементы, то тест пройден
            if (((listTest.size() - listControl.size()) == 0) && (sameElement)) {
                testResult.add("Test 4: Success");
            } else {
                testResult.add("Test 4: Error");
            }

            //Выключаем вебдрайвер
            support.stopDriver();
        } catch (Exception e) {
            testResult.add("Test 4: Exception");
            e.printStackTrace();
            support.stopDriver();
        }
    }

}
